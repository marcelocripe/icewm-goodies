��          t      �                 (  -   D     r     �     �  	     ^     "   x     �  �  �      y  -   �  z   �  �   C     �        *     �   A  "     >   ?                  
                                      	    Add a (manual) command Add application from a list Enter command to be added to the startup file Enter command you want to run at antiX's startup. Note: an ampersand/& will automatically be appended to the end of the command Remove Remove an application Startup ( The line was added to $startupfile. It will start automatically the next time you start  antiX \n $add_remove_text $startupfile): antiX-startup GUI Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-15 17:18+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2023
Language-Team: Portuguese (Brazil) (https://app.transifex.com/anticapitalista/teams/10162/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Adicionar um Comando Manualmente Adicionar um Aplicativo a Partir de uma Lista Insira o comando a ser adicionado ao arquivo de configurações de inicialização ‘startup’ do gerenciador de janelas Insira o comando que você quer executar na inicialização do antiX. Observação: uma barra e um ‘E‘ comercial ‘/&’ serão adicionados automaticamente no final do comando Remover Remover um Aplicativo Arquivo de configurações ‘startup’ ( A nova linha foi adicionada com sucesso no arquivo inicialização $startupfile. Na próxima vez que você iniciar o antiX utilizando esta área de trabalho, o aplicativo relacionado será inicializado automaticamente. \n $add_remove_text $startupfile): Adicionar ou Remover um Aplicativo na Inicialização do antiX 