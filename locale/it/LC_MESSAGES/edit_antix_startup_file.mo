��          t      �                 (  -   D     r     �     �  	     ^     "   x     �  �  �     \  "   z  1   �  �   �     \     d     |  j   �  "   �                       
                                      	    Add a (manual) command Add application from a list Enter command to be added to the startup file Enter command you want to run at antiX's startup. Note: an ampersand/& will automatically be appended to the end of the command Remove Remove an application Startup ( The line was added to $startupfile. It will start automatically the next time you start  antiX \n $add_remove_text $startupfile): antiX-startup GUI Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-15 17:18+0000
Last-Translator: Dede Carli <dede.carli.drums@gmail.com>, 2023
Language-Team: Italian (https://app.transifex.com/anticapitalista/teams/10162/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Aggiungi un comando (manuale) Aggiungi applicazione da un elenco Immetti il comando da aggiungere al file di avvio Immetti il comando che desideri eseguire all'avvio di antiX. Nota: una e commerciale/& verrà automaticamente aggiunta alla fine del comando Rimuovi Rimuovi un'applicazione Avvio ( La riga è stata aggiunta a $startupfile. Si avvierà automaticamente la prossima volta che avvierai antiX \n $add_remove_text $startupfile): GUI avvio-antiX 