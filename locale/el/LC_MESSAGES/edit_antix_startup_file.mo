��          t      �                 (  -   D     r     �     �  	     ^     "   x     �  �  �  >   8  <   w  n   �     #     $  #   5  	   Y  �   c  "        4                  
                                      	    Add a (manual) command Add application from a list Enter command to be added to the startup file Enter command you want to run at antiX's startup. Note: an ampersand/& will automatically be appended to the end of the command Remove Remove an application Startup ( The line was added to $startupfile. It will start automatically the next time you start  antiX \n $add_remove_text $startupfile): antiX-startup GUI Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-15 17:18+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2023
Language-Team: Greek (https://app.transifex.com/anticapitalista/teams/10162/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 Προσθέστε μια (μη αυτόματη) εντολή Προσθήκη εφαρμογής από μια λίστα Εισαγάγετε την εντολή που θα προστεθεί στο αρχείο εκκίνησης Εισαγάγετε την εντολή που θέλετε να εκτελέσετε κατά την εκκίνηση του antiX. Σημείωση: ένα σύμφωνο/& θα προστεθεί αυτόματα στο τέλος της εντολής Αφαίρεση Αφαίρεση εφαρμογής Startup ( Η γραμμή προστέθηκε στο $startupfile. Θα ξεκινήσει αυτόματα την επόμενη φορά που θα ξεκινήσετε το antiX \n $add_remove_text $startupfile): antiX-startup GUI 