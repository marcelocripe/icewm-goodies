# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Roy Avrett <rjavrett@gmail.com>, 2023
# anticapitalista <anticapitalista@riseup.net>, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-28 17:46+0200\n"
"PO-Revision-Date: 2023-02-28 15:49+0000\n"
"Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2023\n"
"Language-Team: Greek (https://www.transifex.com/anticapitalista/teams/10162/el/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: el\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: icewm-remember-settings:108
msgid "HELP"
msgstr "ΒΟΗΘΕΙΑ"

#: icewm-remember-settings:109
msgid ""
"Save the <b>size and position</b>, <b>workspace</b> and <b>layer</b> of a "
"window using the IceWM-remember-settings app."
msgstr ""
"Αποθηκεύστε το <b>μέγεθος και θέση</b>, τον <b>χώρο εργασίας</b> και το "
"<b>επίπεδο</b> ενός παραθύρου χρησιμοποιώντας την εφαρμογή IceWM-remember-"
"settings."

#: icewm-remember-settings:111
msgid ""
"Next time you launch the program, it will remember the window properties "
"last saved."
msgstr ""
"Την επόμενη φορά που θα εκκινήσετε το πρόγραμμα, θα θυμάται τις ιδιότητες "
"του παραθύρου που αποθηκεύτηκαν τελευταία."

#: icewm-remember-settings:113
msgid "You can also delete this information unticking all options."
msgstr ""
"Μπορείτε επίσης να διαγράψετε αυτές τις πληροφορίες, αφαιρώντας όλες τις "
"επιλογές."

#: icewm-remember-settings:114
msgid ""
"Use the <b>Select other</b> option to select a different window/program to "
"configure."
msgstr ""
"Χρησιμοποιήστε την επιλογή <b>Επιλογή</b> άλλου για να επιλέξετε διαφορετικό"
" παράθυρο/πρόγραμμα για διαμόρφωση."

#: icewm-remember-settings:126 icewm-remember-settings:219
msgid "Add/Remove IceWM Window Defaults"
msgstr "Προσθήκη/Αφαίρεση προεπιλογών παραθύρου IceWM"

#: icewm-remember-settings:127
msgid "<b>Select a program. Store it's window properties.</b>"
msgstr ""
"<b>Επιλέξτε ένα πρόγραμμα. Αποθηκεύστε τις ιδιότητες του παραθύρου.</b>"

#: icewm-remember-settings:128
msgid "What window configuration you want antiX to remember/forget?"
msgstr "Ποια διαμόρφωση παραθύρου θέλετε να θυμάται/ξεχάσει το antiX;"

#: icewm-remember-settings:220
#, sh-format
msgid ""
"Entries shown below are for the <b>$appclass</b> ($appname) window.\\n\\nAll"
" options marked will be saved, all unmarked will be deleted.\\n\\n Note: "
"Workspace number shown is the window's current workspace. \\n \tDon't worry "
"that it appears too low.\\n\\n"
msgstr ""
"Οι καταχωρήσεις που εμφανίζονται παρακάτω αφορούν το παράθυρο "
"<b>$appclass</b> ($appname).\\n\\nΌλες οι επιλογές που έχουν επισημανθεί θα "
"αποθηκευτούν, όλες οι μη επισημασμένες θα διαγραφούν.\\n\\n Σημείωση: Ο "
"αριθμός του χώρου εργασίας που εμφανίζεται είναι ο τρέχων χώρος εργασίας του"
" παραθύρου. \\n Μην ανησυχείτε ότι φαίνεται πολύ χαμηλό.\\n\\n"

#: icewm-remember-settings:225
msgid "Select"
msgstr "Επιλέξτε"

#: icewm-remember-settings:225
msgid "Type"
msgstr "Τύπος:"

#: icewm-remember-settings:225
msgid "Value"
msgstr "Αξία"

#: icewm-remember-settings:226
msgid "Geometry"
msgstr "Γεωμετρία"

#: icewm-remember-settings:227
msgid "Layer"
msgstr "Στρώμα"

#: icewm-remember-settings:227
msgid "workspace"
msgstr "χώρο εργασίας"

#: icewm-remember-settings:229
msgid "Select other"
msgstr "Επιλέξτε άλλο"
